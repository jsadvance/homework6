'use strict'

// ## Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

//  Коли виконується асинхронності, програма не чекає, поки виконається певна операція, а замість цього продовжує виконання інших операцій

// ## Завдання
// Написати програму "Я тебе знайду по IP"

// #### Технічні вимоги:
// - Створити просту HTML-сторінку з кнопкою `Знайти по IP`.
// - Натиснувши кнопку - надіслати AJAX запит за адресою `https://api.ipify.org/?format=json`, отримати звідти IP адресу клієнта.
// - Дізнавшись IP адресу, надіслати запит на сервіс `https://ip-api.com/` та отримати інформацію про фізичну адресу.
// - під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// - Усі запити на сервер необхідно виконати за допомогою async await.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Литература:
// - [Async/await](https://learn.javascript.ru/async-await)
// - [async function](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/async_function)
// - [Документація сервісу ip-api.com](http://ip-api.com/docs/api:json)

const root = document.querySelector('#root')
const btn = document.querySelector('.button')

class Request {
  async getInfo() {
    const response = await fetch('https://api.ipify.org/?format=json')
    const { ip } = await response.json()

    const responseInfo = await fetch(
      `http://ip-api.com/json/${ip}?fields=status,country,countryCode,region,regionName,city,continent,timezone&lang=ru`
    )

    return responseInfo.json()
  }
}

class Info {
  render(obj) {
    const { country, city, regionName, continent, timezone } = obj
    const conteiner = document.createElement('div')

    const zoneInfo = document.createElement('p')
    zoneInfo.textContent = `Часовий пояс: ${timezone}`

    const continentInfo = document.createElement('p')
    continentInfo.textContent = `Континент: ${continent}`

    const countryItem = document.createElement('p')
    countryItem.textContent = `Країна: ${country}`

    const regionItem = document.createElement('p')
    regionItem.textContent = `Регион: ${regionName}`

    const cityItem = document.createElement('p')
    cityItem.textContent = `Місто: ${city}`

    conteiner.append(zoneInfo, continentInfo, countryItem, regionItem, cityItem)
    return conteiner
  }
}

const request = new Request()
const info = new Info()

btn.addEventListener('click', (el) => {
  request.getInfo().then((date) => {
    console.log(date)
    root.innerHTML = ''
    root.append(info.render(date))
  })
})
